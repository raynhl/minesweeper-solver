import subprocess
import time
import re
import threading

regex = r"(<=+>)\s(Result: )(WIN|LOSE|TIMEOUT)\s(>=+<)"

configurations = [(9,9), (16,16), (30,30)]
probability = 0.05
probability_step = 0.05

while (probability <= 0.4): 
	for configuration in configurations:
		# Repeat 100 times each.
		for i in range(100):
			print("Configuration {} x {}, Probability {}: Run # {}".format(configuration[0], configuration[1], probability, i))
			start_time = time.time()
			current_game_output = subprocess.check_output(["java", "Minesweeper", str(configuration[0]), str(configuration[1]), str(probability)])
			end_time = time.time() - start_time
			current_game_output = current_game_output.decode("utf-8")

			matches = re.search(regex, r"{}".format(current_game_output))

			if matches:
				result = (matches.group(3))


			with open("results.csv", "a") as f:
				f.write("{},{},{},{},{}\n".format(configuration[0], configuration[1], probability, result, end_time))

	probability += probability_step