public class Move {
	public enum ClickType {
		NORMAL, FLAG
	};

	private Position position;
	private ClickType clickType;

	public Move(Position position, ClickType clickType) {
		this.position = position;
		this.clickType = clickType;
	}

	public Position getPosition() {
		return this.position;
	}

	public ClickType getClickType() {
		return this.clickType;
	}
}