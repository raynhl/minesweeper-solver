import java.util.Vector;
import java.lang.Math;

public class Matrix {
	public enum Solution {
		INFINITE_SOLUTIONS,
		NO_SOLUTIONS,
		UNIQUE_SOLUTION
	};

	private Vector<Vector<Double>> rows;

	public Matrix () {
		this.rows = new Vector<Vector<Double>>();
	}

	public void addRow(Vector<Double> row) {
		if (row != null) {
			Vector<Double> newRow = new Vector<Double>();
			newRow.addAll(row);

			this.rows.add(newRow);
		}
	}

	public Vector<Double> getRow(int index) {
		return this.rows.get(index);
	}

	public int getHeight() {
		return this.rows.size();
	}

	public int getWidth() {
		return this.rows.get(0).size();
	}

	public double getValue(int height, int width) {
		return this.rows.get(height).get(width).doubleValue();
	}

	public void setValue(int row, int col, double value) {
		this.rows.get(row).set(col, new Double(value));
	}

	public void gaussianEliminate() {
		if (this.rows.isEmpty()) {
			return;
		}

		int totalColumns = this.getWidth();
		int totalRows = this.getHeight();

		int row = 0;
		int col = 0;

		while ((row < totalRows) && (col < totalColumns)) {
			int maxRow = row;

			for (int currentRow = row + 1; currentRow < totalRows; currentRow++) {
				if (Double.compare(Math.abs(this.getValue(currentRow, col)), Math.abs(this.getValue(maxRow, col))) > 0) {
					maxRow = currentRow;
				}
			}

			if (Double.compare(this.getValue(maxRow, col), 0.0) != 0) {

				if (row != maxRow) {
					Vector<Double> tempRow = (Vector<Double>) this.rows.get(row).clone();
					this.rows.set(row, (Vector<Double>) this.rows.get(maxRow).clone());
					this.rows.set(maxRow, tempRow);
				}

				// Breakpoint
				double currentValue = this.getValue(row, col);
				this.rows.set(row, this.multiply(this.rows.get(row), 1.0 / currentValue));

				//Break2
				for (int iterRow = row + 1; iterRow < totalRows; ++iterRow) {
					double multiplicationValue = -this.getValue(iterRow, col);

					if (Double.compare(Math.abs(multiplicationValue), 0.0) != 0) {
						this.rows.set(row, this.multiply(this.rows.get(row), multiplicationValue));
						this.rows.set(iterRow, this.add(this.rows.get(iterRow), this.rows.get(row)));
						this.rows.set(row, this.multiply(this.rows.get(row), 1.0 / multiplicationValue));
					}

				}

				row++;
			}

			col++;
		}
	}

	public Vector<Double> multiply(Vector<Double> row, double value) {
		for(int i = 0; i < row.size(); i++) {
			double newValue = row.get(i).doubleValue() * value;
			row.set(i, new Double(newValue));
		}

		return row;
	}

	public Vector<Double> add(Vector<Double> row1, Vector<Double> row2) {
		if (row1.size() != row2.size()) {
			return null;
		}

		Vector<Double> returnVector = new Vector<Double>();

		for (int i = 0; i < row1.size(); i++) {

			returnVector.add(new Double(row1.get(i).doubleValue() + row2.get(i).doubleValue()));
		}

		return returnVector;
	}

	public int getFirstNonZeroRow() {
		for (int row = (this.getHeight() - 1); (row >= 0) && (row < this.getHeight()); --row) {
			Vector<Double> currentRow = this.getRow(row);
			boolean foundNonZero = false;

			for (int col = 0; (col < this.getWidth()) && !foundNonZero; col++) {
				foundNonZero = Double.compare(currentRow.get(col).doubleValue(), 0.0) != 0;
			}

			if (foundNonZero) {
				return row;
			}
		}

		return -1;
	}

	public void print() {
		for (int row = 0; row < getHeight(); row++) {
			for (int col = 0; col < getWidth(); col++) {
				if (col == (getWidth() - 1)) {
					System.out.print("|");
				}

				System.out.format("%2d", (int) getValue(row,col));
				System.out.print(" ");
			}

			System.out.println("");
		}
	}
}