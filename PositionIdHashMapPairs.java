import java.util.HashMap;

public class PositionIdHashMapPairs {
	private HashMap<Integer, Position> idToPositionMap;
	private HashMap<Integer, PositionId> positionToIdMap;

	public PositionIdHashMapPairs(HashMap<Integer, Position> idToPositionMap, HashMap<Integer, PositionId> positionToIdMap) {
		this.idToPositionMap = idToPositionMap;
		this.positionToIdMap = positionToIdMap;
	}

	public HashMap<Integer, Position> getIdToPositionMap() {
		return this.idToPositionMap;
	}

	public HashMap<Integer, PositionId> getPositionToIdMap() {
		return this.positionToIdMap;
	}
}