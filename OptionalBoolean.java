public class OptionalBoolean {
	private boolean value;
	private boolean present;

	public OptionalBoolean() {
		this.present = false;
	}

	public OptionalBoolean(boolean value) {
		this.present = true;
		this.setValue(value);
	}

	public boolean isPresent() {
		return this.present;
	}

	public void setValue(boolean value) {
		this.value = value;
		this.present = true;
	}

	public boolean getValue() {
		return this.value;
	}
}