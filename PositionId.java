public class PositionId {
	private Position position;
	private int id;

	public PositionId(Position position, int id) {
		this.position = position;
		this.id = id;
	}

	public Position getPositionObject() {
		return this.position;
	}

	public int getId() {
		return this.id;
	}
}