import java.util.ArrayList;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Vector;

public class Solver {

	public final int MINE=9;
	public final int CLOSE=-1;  
	public final int BLANK=0;
	private ArrayList<Position> nonCompletedPosition;
	private PositionIdHashMapPairs positionIdHashMapPairs;
	private Minesweeper m;
	private Matrix solutionMatrix;

	private final int[][] adjacentMap = {
		{-1, -1},
		{0, -1},
		{1, -1},
		{1, 0},
		{1, 1},
		{0, 1},
		{-1, 1},
		{-1, 0}
	};

	public Solver() {
	}

	public ArrayList<Move> getMoves(Minesweeper m) {
		// Step (1): List squares that touches non-opened square
		this.m = m;
		this.nonCompletedPosition = getNoneCompletedPositions();
		
		// Step (2): Get all of the adjacent squares that have not been clicked and identify them.
		this.positionIdHashMapPairs = assignIdToPositions();

		if((this.nonCompletedPosition.size() == 0) || (this.positionIdHashMapPairs.getPositionToIdMap().size() == 0)) {
		// There cannot be any solution.
			return null;
		}

		createSolutionMatrix();
		// this.solutionMatrix.print();
		this.solutionMatrix.gaussianEliminate();

		// System.out.println("After gaussian eliminate");
		// this.solutionMatrix.print();
		
		int firstNonZeroRow = this.solutionMatrix.getFirstNonZeroRow();
		int maxVariableColumns = this.solutionMatrix.getWidth() - 1;
		
		Vector<OptionalBoolean> results = new Vector<OptionalBoolean>();

		for (int i = 0; i < maxVariableColumns; i++) {
			results.add(new OptionalBoolean());
		}

		for (int row = firstNonZeroRow; row >= 0 && row <= firstNonZeroRow; row--) {
			boolean failTofindValue = false;
			int pivot = row;
			double pivotValue = this.solutionMatrix.getValue(row, pivot);
			double value = this.solutionMatrix.getValue(row, maxVariableColumns);

			for (int col = row + 1; col < maxVariableColumns; col++) {
				double currentValue = this.solutionMatrix.getValue(row, col);

				if ((Double.compare(pivotValue, 0.0) == 0) && (Double.compare(currentValue, 0.0) != 0)) {
					pivot = col;
					pivotValue = currentValue;
				}

				//breakpoint3
				if (Double.compare(currentValue, 0.0) != 0) {
					if (results.get(col).isPresent()) {
						value -= currentValue * (results.get(col).getValue() ? 1.0 : 0.0);						
						this.solutionMatrix.setValue(row, col, 0.0);
					}

					else {
						failTofindValue = true;
					}
				}
			}

			// breakpoint4
			this.solutionMatrix.setValue(row, maxVariableColumns, value);

			if (Double.compare(pivotValue, 0.0) != 0) {
				if (failTofindValue) {
					double minValue = 0;
					double maxValue = 0;

					for (int col = row; col < maxVariableColumns; col++) {
						double currentValue = this.solutionMatrix.getValue(row, col);

						if (Double.compare(currentValue, 0.0) > 0) {
							maxValue += currentValue;
						}

						if (Double.compare(currentValue, 0.0) < 0) {
							minValue += currentValue;
						}
					}


					if (Double.compare(value, minValue) == 0) {
						for (int col = row; col < maxVariableColumns; col++) {
							double currentValue = this.solutionMatrix.getValue(row, col);

							if (Double.compare(currentValue, 0.0) > 0) {
								results.get(col).setValue(false);
							}

							if (Double.compare(currentValue, 0.0) < 0) {
								results.get(col).setValue(true);
							}
						}
					}

					else if (Double.compare(value, maxValue) == 0) {
						for (int col = row; col < maxVariableColumns; col++) {
							double currentValue = this.solutionMatrix.getValue(row, col);

							if (Double.compare(currentValue, 0.0) > 0) {
								results.get(col).setValue(true);
							}

							if (Double.compare(currentValue, 0.0) < 0) {
								results.get(col).setValue(false);
							}
						}
					}
				}

				else {
					if (results.get(pivot).isPresent()) {
						System.out.println("Found pivot for: " + pivot);
					}

					else {
						System.out.println("Found standard result: " + pivot + "=>" + value);
						
						if (Double.compare(value, 0.0) == 0 || Double.compare(value, 1.0) == 0) {
							if (Double.compare(value, 1.0) == 0) {
								results.get(pivot).setValue(true);
							}

							else {
								results.get(pivot).setValue(false);
							}
						}

						else {
							System.out.println("Found pivot value is not 0 or 1. ???");
						}
					}
				}
			}

			else {
				System.out.println("There's a pivot in row: " + row);
			}
		}

		//breakpoint 5
		ArrayList<Move> moves = new ArrayList<Move>();

		for (int i = 0; i < this.solutionMatrix.getWidth() - 1; i++) {
			if (results.get(i).isPresent()) {
				if (results.get(i).getValue()) {
					moves.add(new Move(this.positionIdHashMapPairs.getIdToPositionMap().get((Integer) i), Move.ClickType.FLAG));
				}

				else {
					moves.add(new Move(this.positionIdHashMapPairs.getIdToPositionMap().get((Integer) i), Move.ClickType.NORMAL));
				}
			}
		}

		for (int i = 0; i < moves.size(); i++) {
			Position tempPosition = moves.get(i).getPosition();
			System.out.print("Move #" + i + ": (");
			System.out.print(tempPosition.getX() + ", " + tempPosition.getY() + ")");

			if (moves.get(i).getClickType() == Move.ClickType.FLAG) {
				System.out.println(" FLAG ");
			}

			else if (moves.get(i).getClickType() == Move.ClickType.NORMAL) {
				System.out.println(" NORMAL ");
			}
		}

		return moves;	
	}

	private ArrayList<Position> getNoneCompletedPositions() {
		ArrayList<Position> nonCompletedPosition = new ArrayList<Position>();
		int unopenedCount = 0;
		for (int row = 0; row < this.m.getHeight(); row++) {
			for (int col = 0; col < this.m.getWidth(); col++) {

				Position position = new Position(row, col);

				if (this.m.getSquareState(position.getX(),position.getY()) != CLOSE) {
					// There's a hint!
					if ((this.m.getSquareState(position.getX(),position.getY()) > BLANK) && (this.m.getSquareState(position.getX(),position.getY()) < MINE)) {
						boolean unopenedFound = false;;

						for (int i = 0; i < 8 && !unopenedFound; i++) {
							Position tempPosition = new Position(row + adjacentMap[i][0], col + adjacentMap[i][1]);
							
							if (this.m.isValidPosition(tempPosition)) {
								if (this.m.getSquareState(tempPosition.getX(), tempPosition.getY()) == this.m.CLOSE) {
									unopenedFound = true;
									unopenedCount++;
								}
							}
						}

						if (unopenedFound) {
							nonCompletedPosition.add(nonCompletedPosition.size(), position);
						}
					}
				}
			}
		}

		return nonCompletedPosition;
	}

	private PositionIdHashMapPairs assignIdToPositions() {
		int currentSquareId = 0;

		HashMap<Integer, Position> idToPositionMap = new HashMap<Integer,Position>();
		HashMap<Integer, PositionId> positionToIdMap = new HashMap<Integer, PositionId>();

		// Look around a marked square to see if there's any non-opened square
		for (int i = 0; i < this.nonCompletedPosition.size(); i++) {
			for (int j = 0; j < 8; j++) {
				Position currentPosition = this.nonCompletedPosition.get(i);
				Position tempPosition = new Position(currentPosition.getX() + adjacentMap[j][0], currentPosition.getY() +  + adjacentMap[j][1]);

				if (this.m.isValidPosition(tempPosition)) {
					// Found a non-opened square!
					if (this.m.getSquareState(tempPosition.getX(),tempPosition.getY()) == this.m.CLOSE) {

						PositionId positionId = new PositionId(tempPosition, currentSquareId);

						if (!positionToIdMap.containsKey(tempPosition.getHash(this.m))) {
							positionToIdMap.put(tempPosition.getHash(this.m), positionId);
							idToPositionMap.put(currentSquareId, tempPosition);
							currentSquareId++;
						}
					}
				}
			}
		}

		return new PositionIdHashMapPairs(idToPositionMap, positionToIdMap);
	}


	private void createSolutionMatrix() {

		Matrix solutionMatrix = new Matrix();

		for (int i = 0; i < this.nonCompletedPosition.size(); i++) {
			Vector<Double> temporaryRow = new Vector<Double>(this.positionIdHashMapPairs.getPositionToIdMap().size() + 1);
			
			for (int j = 0; j < (this.positionIdHashMapPairs.getPositionToIdMap().size() + 1); j++) {
				temporaryRow.add(new Double(0.0));
			}

			Position tempPosition = this.nonCompletedPosition.get(i);
			temporaryRow.set(this.positionIdHashMapPairs.getPositionToIdMap().size(), new Double(this.m.getSquareState(tempPosition.getX(),tempPosition.getY())));

			for (int k = 0; k < 8; k++) {
				Position adjacent = new Position(tempPosition.getX() + adjacentMap[k][0], tempPosition.getY() + adjacentMap[k][1]);

				if (this.m.isValidPosition(adjacent)) {
					if (this.m.getSquareState(adjacent.getX(), adjacent.getY()) == this.m.CLOSE) {
						HashMap<Integer, PositionId> tempPositionToIdMap = this.positionIdHashMapPairs.getPositionToIdMap();
						int matrixColumn = tempPositionToIdMap.get(adjacent.getHash(this.m)).getId();
						
						temporaryRow.set(matrixColumn, new Double(1.0));
					}

					else if (this.m.getSquareState(adjacent.getX(), adjacent.getY()) == this.m.FLAGGED) {
						temporaryRow.set(this.positionIdHashMapPairs.getPositionToIdMap().size(), new Double(temporaryRow.get(this.positionIdHashMapPairs.getPositionToIdMap().size()) - 1));
					}
				}
			}
			solutionMatrix.addRow(temporaryRow);
		}

		this.solutionMatrix = solutionMatrix;

	}

	public PositionIdHashMapPairs getPositionIdHashMapPairs() {
		return this.positionIdHashMapPairs;
	}
}