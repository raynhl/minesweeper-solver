public class Position {
	private int x;
	private int y;

	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	public boolean equals(Position position) {
		if (this.x != position.getX()) {
			return false;
		}

		if (this.y != position.getY()) {
			return false;
		}

		return true;
	}

	public int getHash(Minesweeper m) {
		return this.x * m.getHeight() + this.y;
	}
}
