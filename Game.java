import java.util.TreeSet;
import java.util.Random;
import java.util.ArrayList;

public class Game {
	private GameState gameState;
	private Minesweeper m;
	private int numberOfMines;
	private int squaresLeft;

	public Game(Minesweeper m) {
		this.m =  m;
		this.gameState = GameState.PROGRESS;
		this.numberOfMines = this.m.getAllMineLocation(this.m.getMineMap()).size();
	}

	public void print() {
		this.m.printGameMap();
	}

	public void acceptMove(Move move) {
		if (getGameState() == GameState.PROGRESS) {
			this.gameState = this.handleMove(move);
		}
	}

	public boolean isMovesAllFlag(ArrayList<Move> moves) {
		for (int i = 0; i < moves.size(); i++) {
			if (moves.get(i).getClickType() == Move.ClickType.NORMAL) {
				return false;
			}
		}

		return true;
	}

	private GameState handleMove(Move move) {
		GameState resultingState = GameState.PROGRESS;

		Position movePosition = move.getPosition();

		if (this.m.isValidPosition(movePosition)) {
			if (move.getClickType() == Move.ClickType.NORMAL) {
				boolean isNotMine = this.m.openSquare(movePosition.getX(), movePosition.getY());
				this.m.setClosedSquaresLeft(this.m.getClosedSquaresLeft() - 1);

				if (!isNotMine) {
					resultingState = GameState.LOST;
				}

				else if(this.m.getClosedSquaresLeft() == this.numberOfMines) {
					resultingState = GameState.WON;
				}

			}

			else if (move.getClickType() == Move.ClickType.FLAG) {
				// System.out.println("FLAGGING");
			}
		}

		else {
			System.out.println("[ERROR] Invalid position!");
		}

		return resultingState;
	}

	public void setGameState(GameState gameState) {
		this.gameState = gameState;
	}

	public GameState getGameState() {
		return this.gameState;
	}

	public Minesweeper getMinesweeper() {
		return this.m;
	}

	public int getNumberOfMines() {
		return this.numberOfMines;
	}

	public void checkIfGameWon(Solver solver) {
		if (getGameState() == GameState.LOST) {
			return;
		}

		else if (solver.getPositionIdHashMapPairs().getIdToPositionMap().size() == this.getNumberOfMines()) {
			setGameState(GameState.WON);
			return;
		}
	}
}